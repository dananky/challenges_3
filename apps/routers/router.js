const verifySignUp = require("./verifySignUp");
const authJwt = require("./verifyJwtToken");
const authController = require("../controllers/authController.js");
const userController = require("../controllers/userController.js");
const bookController = require("../controllers/bookController.js");
const commentController = require("../controllers/commentController.js");

module.exports = function(app) {
  // Auth
  app.post(
    "/api/auth/signup",
    [
      verifySignUp.checkDuplicateUserNameOrEmail,
      verifySignUp.checkRolesExisted
    ],
    authController.validate("signup"),
    authController.signup
  );
  app.post("/api/auth/signin", authController.signin);

  // get 1 user according to roles
  app.get("/api/test/user", [authJwt.verifyToken], userController.userContent);
  app.get(
    "/api/test/pm",
    [authJwt.verifyToken, authJwt.isPmOrAdmin],
    userController.managementBoard
  );
  app.get(
    "/api/test/admin",
    [authJwt.verifyToken, authJwt.isAdmin],
    userController.adminBoard
  );

  // user
  app.get("/api/users", [authJwt.verifyToken], userController.users);
  app.put("/users/edit", [authJwt.verifyToken], userController.editRole);
  app.delete("/users/:id", [authJwt.verifyToken], userController.deleteUser);

  // profile
  app.get("/profile", [authJwt.verifyToken], userController.showProfile);
  app.put("/profile/edit", [authJwt.verifyToken], userController.editProfile);

  // books
  app.post("/books", [authJwt.verifyToken], bookController.addBook);
  app.get("/books", [authJwt.verifyToken], bookController.viewBook);
  app.get("/books/:id", [authJwt.verifyToken], bookController.viewBookId);
  app.put("/books/:id", [authJwt.verifyToken], bookController.updateBook);
  app.delete("/books/:id", [authJwt.verifyToken], bookController.deleteBook);
  app.get(
    "/books/detail/:id",
    [authJwt.verifyToken],
    bookController.detailBook
  );

  // comment
  app.post(
    "/comments/:id",
    [authJwt.verifyToken],
    commentController.addComment
  );

  //orders
  // app.post("/orders", [authJwt.verifyToken], orderController.addOrder);
  // app.get("/orders", [authJwt.verifyToken], orderController.viewOrder);
  // app.get("/orders/:userid", [authJwt.verifyToken], orderController.viewOrderUserId);

  // error handler 404
  app.use(function(req, res, next) {
    return res.status(404).send({
      status: 404,
      message: "Not Found"
    });
  });

  // error handler 500
  app.use(function(err, req, res, next) {
    return res.status(500).send({
      error: err
    });
  });
};
