const db = require("../configs/db.js");
const Book = db.book;
const User = db.user;
const Comment = db.comment;
const asyncMiddleware = require("express-async-handler");

exports.addBook = asyncMiddleware(async (req, res) => {
  const book = await Book.create({
    title: req.body.title,
    author: req.body.author,
    published_date: req.body.published_date,
    pages: req.body.pages,
    language: req.body.language,
    publisher_id: req.body.publisher_id
  });
  res.status(201).send({
    status: "Book has been inserted",
    data: book
  });
});

exports.viewBook = asyncMiddleware(async (req, res) => {
  const book = await Book.findAll({
    order: [["id", "ASC"]]
  });
  res.status(200).send({
    status: "Book found",
    data: book
  });
});

exports.viewBookId = asyncMiddleware(async (req, res) => {
  const id_book = req.params.id;
  const book = await Book.findOne({
    where: {
      id: id_book
    }
  });
  res.status(200).send({
    status: "Book found",
    data: book
  });
});

exports.updateBook = asyncMiddleware(async (req, res) => {
  const id_book = req.params.id;
  const book = await Book.update(
    {
      title: req.body.title,
      author: req.body.author,
      published_date: req.body.published_date,
      pages: req.body.pages,
      language: req.body.language,
      publisher_id: req.body.publisher_id
    },
    {
      where: {
        id: id_book
      }
    }
  );
  res.status(200).send({
    status: "Book has been updated",
    data: book
  });
});

exports.deleteBook = asyncMiddleware(async (req, res) => {
  const id_book = req.params.id;
  const book = await Book.destroy({
    where: {
      id: id_book
    }
  });
  res.status(200).send({
    status: "Book has been deleted"
  });
});

exports.detailBook = asyncMiddleware(async (req, res) => {
  try {
    const detail = await Book.findAll({
      where: {
        id: req.params.id
      },
      attributes: [
        "id",
        "title",
        "author",
        "published_date",
        "pages",
        "language",
        "publisher_id"
      ],
      include: [
        {
          model: Comment,
          attributes: ["id", "comment", "createdAt"],
          include: {
            model: User,
            attributes: ["username"]
          }
        }
      ],
      order: [[{ model: Comment }, "id", "DESC"]]
    });
    res.status(200).json({
      description: "Book detail found!",
      data: detail
    });
  } catch (error) {
    res.status(500).send({
      error: error,
      message: "Book detail cannot be fetched!"
    });
  }
});
