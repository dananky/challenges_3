const db = require("../configs/db.js");
const Order = db.orders;
const asyncMiddleware = require("express-async-handler");

exports.addOrder = asyncMiddleware(async (req, res) => {
    const userId = req.body.userId;
    if(userId){
        const order = await Order.create({
            userId: req.body.userId,
            bookId: req.body.bookId
        });
        res.status(201).send({
            status: "Order has been inserted",
            data: order
        });
    }else{
        res.status(201).send({
            status: "User Id must be inserted"
        });
    }
    
});

exports.viewOrder = asyncMiddleware(async (req, res) => {
    const order = await Order.findAll({});
    res.status(200).send({
        status: "Order found",
        data: order
    });
});

exports.viewOrderUserId = asyncMiddleware(async (req, res) => {
    const userId = req.params.userid;
    const order = await Order.findAll({
        where:{
            userId: userId
        }
    });
    res.status(200).send({
        status: "Order found",
        data: order
    });
});