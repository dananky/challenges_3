const db = require("../configs/db.js");
const User = db.user;
const Role = db.role;
const User_Roles = db.user_roles;
const asyncMiddleware = require("express-async-handler");
var bcrypt = require("bcryptjs");

exports.users = asyncMiddleware(async (req, res) => {
  const user = await User.findAll({
    attributes: ["id", "name", "username", "email"],
    include: [
      {
        model: Role,
        attributes: ["id", "name"],
        through: {
          attributes: ["userId", "roleId"]
        }
      }
    ],
    order: [["id", "ASC"]]
  });
  res.status(200).json({
    description: "All User",
    user: user
  });
});

exports.editRole = asyncMiddleware(async (req, res) => {
  const user_roles = await User_Roles.update(
    {
      roleId: req.body.roleId
    },
    {
      where: {
        userId: req.body.userId
      }
    }
  );
  res.status(201).send({
    data: user_roles,
    message: "User role successfuly updated"
  });
});

exports.showProfile = asyncMiddleware(async (req, res) => {
  const profile = await User.findOne({
    where: {
      id: req.userId
    }
  });

  res.status(200).send({
    status: "Profile found",
    data: profile
  });
});

exports.editProfile = asyncMiddleware(async (req, res) => {
  try {
    const user = await User.findOne({
      where: {
        id: req.userId
      }
    });

    if (!user) {
      return res.status(404).send({
        error: true,
        message: "User Not Found!"
      });
    }

    const passwordIsValid = bcrypt.compareSync(
      req.body.password,
      user.password
    );
    console.log(passwordIsValid);
    if (!passwordIsValid) {
      return res.status(401).send({
        error: true,
        message: "Invalid Password!"
      });
    }
    const name = req.body.name;

    if (!name) {
      res.status(411).send({
        error: error,
        message: "Name cannot null!"
      });
    }

    const profile = await User.update(
      {
        name: name
      },
      {
        where: {
          id: req.userId
        }
      }
    );
    res.status(200).send({
      message: "Profile has been updated",
      data: profile
    });
  } catch (error) {
    res.status(500).send({
      error: error,
      message: "Failed to edit profile"
    });
  }
});

exports.deleteUser = asyncMiddleware(async (req, res) => {
  const user = await User.findOne({
    where: {
      id: req.params.id
    }
  });

  if (!user) {
    return res.status(404).send({
      error: true,
      message: "User Not Found!"
    });
  }

  await User.destroy({
    where: {
      id: user.id
    }
  });

  res.status(200).send({
    message: "User successfully deleted"
  });
});

exports.userContent = asyncMiddleware(async (req, res) => {
  const user = await User.findOne({
    where: { id: req.userId },
    attributes: ["name", "username", "email"],
    include: [
      {
        model: Role,
        attributes: ["id", "name"],
        through: {
          attributes: ["userId", "roleId"]
        }
      }
    ]
  });
  res.status(200).json({
    description: "User Content Page",
    user: user
  });
});

exports.adminBoard = asyncMiddleware(async (req, res) => {
  const user = await User.findOne({
    where: { id: req.userId },
    attributes: ["name", "username", "email"],
    include: [
      {
        model: Role,
        attributes: ["id", "name"],
        through: {
          attributes: ["userId", "roleId"]
        }
      }
    ]
  });
  res.status(200).json({
    description: "Admin Board",
    user: user
  });
});

exports.managementBoard = asyncMiddleware(async (req, res) => {
  const user = await User.findOne({
    where: { id: req.userId },
    attributes: ["name", "username", "email"],
    include: [
      {
        model: Role,
        attributes: ["id", "name"],
        through: {
          attributes: ["userId", "roleId"]
        }
      }
    ]
  });
  res.status(200).json({
    description: "Management Board",
    user: user
  });
});
