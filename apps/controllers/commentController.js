const db = require("../configs/db.js");
const Comment = db.comment;
const asyncMiddleware = require("express-async-handler");

exports.addComment = asyncMiddleware(async (req, res) => {
  try {
    let commentValue = req.body.comment;
    if (!commentValue) {
      res.status(411).send({
        error: error,
        message: "Comment required"
      });
    }

    const comment = await Comment.create({
      bookId: req.params.id,
      userId: req.userId,
      comment: commentValue
    });
    res.status(201).send({
      message: "Comment has been inserted",
      data: comment
    });
  } catch (error) {
    res.status(500).send({
      error: error,
      message: "Failed to add comment"
    });
  }
});
