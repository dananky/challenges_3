module.exports = (sequelize, Sequelize) => {
        const User_Roles = sequelize.define('user_roles', {
            status: {
                type: Sequelize.STRING,
                defaultValue: "unblocked"
            }
        });
        return User_Roles;
    }